/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.interconnect.userregister;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSetMetaData;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author darklight
 */
public class CompanyFunctions {
    private static String dbURL = "jdbc:derby://localhost:1527/webclients;create=true;user=master;password=master";
    // jdbc Connection
    private static Connection conn = null;
    private static Statement stmt = null;
    
    public CompanyFunctions() {
        createConnection();
        
    }

    private static void createConnection() {
        try
        {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            //Get a connection
            conn = DriverManager.getConnection(dbURL); 
        }
        catch (Exception except)
        {
            except.printStackTrace();
        }
    }
    
    public void insertEntry(String id) {
        try
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");  
            LocalDateTime now = LocalDateTime.now();  
            stmt = conn.createStatement();
            stmt.execute("insert into REGISTROS values ('" +
                    id + "','" + dtf.format(now) +"')");
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }
    
    public void selectRegistros(PrintWriter out)
    {
        try
        {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("select * from REGISTROS");
            
            out.println("<table><tr>");

            out.println("<td>" + "ID" + "</td>" + "<td>" + "CEDULA" + "</td>" + "<td>" + "NOMBRE" + "</td>" + "<td>" + "FECHA" + "</td>");  

            out.println("</tr>");
            
            while(results.next())
            {
                String id = results.getString("ID");
                String date = results.getString("FECHA");
                //
                ArrayList<String> user = new ArrayList<>();
                
                user = searchID(id);
                
                if (user != null) {
                    String nameuser = user.get(2)+" "+user.get(3);
                    String ccuser = user.get(1);
                    
                    out.println("<tr><td>" + id + "</td>" + "<td>" + ccuser + "</td>" + "<td>" + nameuser + "</td>" + "<td>" + date + "</td></tr>");
                }
            }
            out.println("</table>");
            results.close();
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }
    
    public ArrayList<String> searchID(String id) {
        try
        {
            System.out.println(id);
            stmt = conn.createStatement();
            ResultSet user = stmt.executeQuery("select * from USUARIOS where ID =" + "'" + id + "'");
            
            ArrayList<String> userString = new ArrayList<>();
            
            if (user.next()) {
                userString.add(user.getString("ID"));
                userString.add(user.getString("CEDULA"));
                userString.add(user.getString("NOMBRES"));
                userString.add(user.getString("APELLIDOS"));
                
            }
            return userString;
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
            return null;
        }
    }

    public void shutdown()
    {
        try
        {
            if (stmt != null)
            {
                stmt.close();
            }
            if (conn != null)
            {
                DriverManager.getConnection(dbURL + ";shutdown=true");
                conn.close();
            }           
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }

    }

}
